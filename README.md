
	
一、预备知识
	针对本文章，读者需要了解如下几方面的内容：
	1. Regulator 子系统相关的接口；
	2. 熟悉platform 驱动开发；
	3. 熟悉iic驱动开发；
	4. 熟悉device驱动子系统中属性文件的创建等接口
	5. 熟悉regmap相关的接口调用
	
二、 regulator device 驱动开发流程简要说明
2.1 regulator device驱动开发流程
	1. 创建struct regulator_desc类型的变量，包括regulator device的类型、寄存器定义、操作函数（struct regulator_ops类型
           的变量）；
	2. 创建struct regulator_config类型的变量，包括regulator device相关的约束信息、consumer信息、regmap/enable gpio等相关信息；
	3. 调用regulator_register，完成regulator device的注册。

2.2 regulator consumer的开发
	1. 通过regulator_get接口，依据supply name、device name在regulator_map查找是否存在对应的regulator_map信息，若存在则获取
           到对应的regulator_dev，并创建struct regulator 类型的变量；
	2. 通过regulator_get获取到struct regulator类型的变量后，则可以进行regulator的enable/disable等操作；
	3. 通过regulator_put释放regulator。

三、virt regulator device简要介绍
	本次我们需要取得的虚拟regulator device是一个iic设备，该设备包含两个寄存器：
	1. 寄存器0x01表示电压输出选择寄存器，其中bit[7:4]表示输出电压的select id，支持8个电压输出选择，最小输出电压为2000000，电压
           调节步进为200000uV
	2. 寄存器0x2表示电压输出的使能与去使能，其中bit4为1表示电压输出使能；0表示关闭电压输出




四、virt regulator device driver设计
针对virt regulator device driver的设计，我们主要分为如下两部分：
	1. 创建i2c device、i2c driver，主要进行virt regulator device的驱动，在i2c driver的probe接口中，完成regulator device
          相关的约束信息的配置、consumer相关的配置信息、iic client对应的regmap的创建，并将这些信息作为platform device init data
          数据，创建对应的platform device；
	2. 完成regulator device platform driver，在该platform driver的probe接口中，依据platform device init data、struct 
           regulator_desc类型变量（包括voltage sel寄存器、voltage sel mask、enable reg、enable reg mask、struct regulator_ops
           等配置），调用regulator_register，完成regulator_dev的创建及注册；

	
	相关数据结构：
	1. virt_regulator_plat_data_t主要用于platform device相关的initdata信息，包含struct regulator_init_data类型变量
         （用于regulator_device相关的约束信息、consumer信息）、regmap（主要表示该virt regulator iic client对应的regmap，
          从而在regulator platform device driver中隐藏对virt regulator访问的真实接口信息（直接使用regmap_read、regmap_write
          即可访问该virt regulator））；
	2. struct virt_regulator_dev主要表示一个虚拟的regulator device，包含regulator_dev成员
	
	
	regulator_desc定义
	因为我们定义的virt regulator提供电压的输出配置、电压输出的使能等内容，且这些操作可直接通过
        操作regmap实现，因此，此处的list_voltage、map_voltage、get_voltage_sel等接口直接使用regulator子系统定义的接口即可，
        无须额外实现这些接口。
	
	
	regulator_init_data的定义
	针对该virt regulator，其电压约束信息为2800000uV，有一个consumer，该consumer的的supply 
类型名称为virt-vcc，consumer对应的设备名称为virt_consumer01；
	
	
	
	sysfs属性文件
	因我们是virt regulator device，我们只能通过借助sysfs来作为virt regulator device相关的寄存器
        的通信机制；此处主要创建voltage_sel、voltage_enable两个属性文件，用于dump virt regulator device的电压输出选择、电压
         使能信息。
	
	
五、virt regulator consumer实现
	为了验证该virt regulator device，我们创建一个简单的platform device，在该platform device 
driver的probe接口中完成regulator的获取以及regulator device的电压输出使能操作；而在该platform device driver的remove接口中完成regulator的释放以及关闭regulator device的电压输出。
代码实现也比较简单，如下图所示：

六、构建流程：
1. 在顶层目录执行make、make install命令即可。然后在images目录下即为本次测试的驱动镜像。
七、验证说明
本次验证的驱动包括如下几个：
	1. virt_i2c_controller目录下存储的是虚拟iic控制器驱动代码，实现虚拟iic控制器模拟、virt regulator device模拟；
	2. virt0701_driver目录下存储的是virt regulator对应的iic driver以及virt regulator对应的platform device；
	3. virt_regulator_chip目录下存储的是virt regulator对应的platform device driver；
	4. virt_consumer目录下存储的是virt regulator的consumer。

验证步骤：
	1. 执行insmod virt_i2c_controller.ko；
	2. 执行insmod virt0701_driver.ko；
	3. 执行insmod virt_regulator_chip.ko；
	此时在目录/sys/bus/i2c/devices/0-0030下，可通过voltage_sel、voltage_enable查看virt regulator相关的输出电压选择、电压输出使能信息，如下所示，此时该regulator device还没有使能，而voltager sel中已经根据上述驱动中定义的regulator device的约束信息，将输出电压设置为280000uV.
	
	然后执行命令insmod virt_consumer.ko，此时电压输出使能已设置

